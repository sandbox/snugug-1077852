<?php
// $Id$
/**
 * 
 * @file
 * A single function API call for adding a node to Instapaper
 */

/**
 *
 * Custom Function to make an Instapaper API call
 * API call takes two variables, the Operator ($op) and the Data
 * The Function will return a standard object with items code, url, and title
 *  $results->code will provide the response code (documented below)
 *  $results->url will provide the saved URL on a successful save (only for $op ADD)
 *  $results->title will provide the saved TITLE on a successful save (only for $op ADD)
 * All data should be sanitized before being sent to the function
 * 
 * $op must be either AUTH or ADD
 * $data must be an array (usage example below)
 * 
 * Authentication Example:
 * $op = 'AUTH';
 * $data = array ('username' => 'Required. Enter Username Here, Can Be Variable',
 *                'password' => 'Required. Enter Password Here, Can Be Variable',);
 * $results = instapaper_api_call($op, $data);
 *
 * Add Example:
 * $op = 'ADD';
 * $data = array ('username' => 'Required. Enter Username Here, Can Be Variable',
 *                'password' => 'Required. Enter Password Here, Can Be Variable',
 *                'url' => 'Required. Enter URL To Be Added Here, Can Be Variable'
 *                'title' => 'Optional. Plain Text, no HTML or UTF-8. If omitted will be automatically generated',
 *                'selection' => 'Optional, Plain Text, no HTML or UTF-8. Will show up as the description under an item in the interface. Can be used to describe where the item came from',
 *                'redirect' => 'close' //Optional. Specifies that, instead of returning the status code, the resulting page should show an HTML “Saved!” notification that attempts to close its own window with Javascript after a short delay. This is useful if you’re sending people directly to /api/add URLs from a web application.');
 * $results = instapaper_api_call($op, $data);
 *
 * Responses for AUTH Method
 *  200: Username and Password Combination OK
 *  403: Invalid Username or Password
 *  500: The service encountered an error
 * Responses for ADD Method
 *  201: URL has been added to the Instaaper account
 *  400: Bad request or exceded rate limit, probably missing required parameter, such as url
 *  403: Invalid Username or Password
 *  500: The service encountered an error
 *  
 */

function instapaper_api_call($op, $data) {
  // Global variables for API variables
  $instapaperAddURL = "https://www.instapaper.com/api/add";
  $instapaperAuthenticateURL = "https://www.instapaper.com/api/authenticate";
  
  // Determine which URL to use
  switch ($op) {
    case 'AUTH': $url = $instapaperAuthenticateURL;
                 break;
    case 'ADD': $url = $instapaperAddURL;
                 break;
    default: return 500;
  }
  
  // Format data recieved into a URL Encode
  $requestData = drupal_query_string_encode($data);
  
  // Format the Drupal HTTP Request
  $request = drupal_http_request(
    url($url, array(absolute => TRUE)), // The URL to be requested
    array('Content-Type' => 'application/x-www-form-urlencoded'), // Header Information
    'POST', //Type of Request (POST or GET)
    $requestData // Data being sent
  );
  
  // Create object of results
  $results->code = $request->code;
  $results->url = $request->headers['Content-Location'];
  $results->title = $request->headers['X-Instapaper-Title'];
  
  return ($results);
  
}

