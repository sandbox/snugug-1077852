<?php
// $Id$
/**
 *
 * @file
 * Allows users to add selected node to their Instapaper que
 */

/**
 * Implements hook_perm().
 */
function instapaper_node_perm() {
  return array(
    'manage instapaper settings',
  );
}

/**
 *
 * Implements hook_menu().
 */
function instapaper_node_menu() {
  $items['admin/config/instapaper'] = array(
    'title' => 'Instapaper Settings',
    'description' => 'Administer the settings for Instapaper',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('instapaper_node_settings_form'),
    'access arguments' => array('manage instapaper settings'),
    'file' => 'instapaper_node.admin.inc'
  );

  return $items;
}

/**
 *
 * Implements hook_user().
 */
function instapaper_node_user($op, &$edit, &$account, $category = NULL) {
  switch ($op) {
    case 'form':
      return instapaper_node_user_form($account);
    case 'validate':
      return instapaper_node_user_form_validate_snug($edit);
    case 'view':
      return instapaper_node_view($account);
  }
}

/**
 *
 * Custom function for adding sharing information to User View
 * Creates a custom view for the User
 */
function instapaper_node_view(&$account) {
  if ($account->instapaper_username != '') {
    $social .= "Instapaper ";
  }

  $account->content['social_media'] = array(
    '#type' => 'user_profile_category',
    '#title' => t('Active Sharing Accounts'),
    '#weight' => 5,
  );

  $account->content['social_media']['items'] = array(
    '#type' => 'user_profile_item',
    '#value' => $social,
  );
}

/**
 *
 * Implements hook_form().
 * Creates a Form for the User view
 */
function instapaper_node_user_form($account) {
  $form['instapaper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Instapaper Account Information'),
    '#weight' => 10,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['instapaper']['instapaper_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => filter_xss($account->instapaper_username),
    '#description' => t('Please enter your Instapaper Username or Email Address'),
    '#weight' => 0,
  );
  $form['instapaper']['instapaper_password'] = array(
    '#type' => 'textfield',
     '#title' => t('Password'),
    '#default_value' => filter_xss($account->instapaper_password),
    '#description' => t('Password, if you have one'),
    '#weight' => 1,
  );

  //  Finds Node Types and stores in Array
  return $form;
}

/**
 *
 * Implements hook_form_validate() for instapaper_node_user_form().
 * Validates the User Information
 */
function instapaper_node_user_form_validate($form, &$form_state) {
  $op = 'AUTH';
  $data = array(
    'username' => filter_xss($form['instapaper_username']),
    'password' => filter_xss($form['instapaper_password']),
  );

  $results = instapaper_api_call($op, $data);

  switch ($results->code) {
    case 200: // Correct Username and Password Combination
      break;
    case 403:
      form_set_error('instapaper_username', 'Invalid Username or Password');
      form_set_error('instapaper_password', '');
      break;
    case 500:
      form_set_error('instapaper_username', 'An error has occurred, please try again later');
      form_set_error('instapaper_password', '');
      break;
  }
}

/**
 *
 * Implements hook_nodeapi().
 * Adds a form (for logged out users) plus a button to add the node to their Instapaper account
 */
function instapaper_node_nodeapi(&$node, $op) {
  switch ($op) {
    case 'view':
      $node->content['instapaper']['#value'] = drupal_get_form('instapaper_node_form', $node);
      $node->content['instapaper']['#weight'] = 5;
      return 0;
  }
}

/**
 *
 * Implements hook_form().
 * Creates a Form for the Node view
 */
function instapaper_node_form($form, $form_state, &$node) {
  // Get Instapaper Fields

  if (isset($node->type)) { //Finds if Node Type is set
    $types = variable_get('instapaper_node_types', array()); // Finds Allowed Node Types
    if ($types[$node->type]) { // Sorts based on Allowed Node Types
      if (user_is_anonymous()) {
        $form['instapaper'] = array(
          '#type' => 'fieldset',
          '#title' => t('Instapaper Account Information'),
          '#weight' => 0,
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        );
        $form['instapaper']['instapaper_username'] = array(
          '#type' => 'textfield',
          '#title' => t('Username'),
          '#default_value' => filter_xss($account->instapaper_username),
          '#description' => t('Please enter your Instapaper Username or Email Address'),
          '#weight' => 0,
        );
        $form['instapaper']['instapaper_password'] = array(
          '#type' => 'textfield',
          '#title' => t('Password'),
          '#default_value' => filter_xss($account->instapaper_password),
          '#description' => t('Password, if you have one'),
          '#weight' => 1,
        );
      }

      $form['instapaper']['instapaper_url'] = array(
        '#type' => 'hidden',
        '#title' => t('URL'),
        '#value' => '<node>',
        '#description' => t("The URL you want to save. Use <node> to use the current node"),
        '#weight' => 2,
      );
      $form['instapaper']['instapaper_title'] = array(
        '#type' => 'hidden',
        '#title' => t('Title'),
        '#value' => '<title>',
        '#description' => t("The Title you want to call your new addition. Use <title> to use the current node's title"),
        '#weight' => 3,
      );
      $form['instapaper']['instapaper_selection'] = array(
        '#type' => 'hidden',
        '#title' => t('Selection'),
        '#value' => '<teaser>', //filter_xss(variable_get('Instapaper_Selection', '<teaser>')),
        '#description' => t("The Selection you want to add. Use <teaser> to use the current node's teaser"),
        '#weight' => 4,
      );
      $form['instapaper_add'] = array(
        '#type' => 'button',
        '#default_value' => variable_get('instapaper_node_label', array()),
        '#weight' => 5,
        '#op' => 'ADD'
      );
    }
  }
  return $form;
}

/**
 *
 * Implements hook_form_validate() for instapaper_node_form().
 * Validates the Submission for Instapaper
 */
function instapaper_node_form_validate(&$element, &$form_state) {

  $op = 'ADD';
  $data = array(
    'username' => filter_xss($form_state['values']['instapaper_username']),
    'password' => filter_xss($form_state['values']['instapaper_password']),
    'url' => filter_xss($form_state['values']['instapaper_url']),
    'title' => filter_xss($form_state['values']['instapaper_title']),
    'selection' => filter_xss($form_state['values']['instapaper_selection']),
  );

  // Replace Defaults
  if ($user->uid != 0) {
    $data['username'] = filter_xss($user->instapaper_username);
    $data['password'] = filter_xss($user->instapaper_password);
  }
  if ($data['url'] == '<node>') {
    global $base_root;
    $data['url'] = url($base_root . '/node/' . $element['#parameters']['2']->nid);
  }
  if ($data['title'] == '<title>') {
    $data['title'] = filter_xss($element['#parameters']['2']->title);
  }
  if ($data['selection'] == '<teaser>') {
    $data['selection'] = strip_tags($element['#parameters']['2']->teaser);
  }

  $results = instapaper_api_call($op, $data);

  switch ($results->code) {
    case 201:
      drupal_set_message($results->url . ' saved as ' . $results->title . ' in your Instapaper account!');
      break;
    case 400:
      form_set_error('instapaper_username', 'A bad request was made to Instapaper');
      break;
    case 403:
      if ($user->uid != 0) {
        form_set_error('instapaper_username', 'Invalid Username or Password');
      }
      else {
        form_set_error('instapaper_username', 'Invalid Username or Password. Please check your Instapaper account information in your profile');
      }
      break;
    case 500:
      form_set_error('instapaper_username', 'An error has occurred, please try again later');
      break;
  }
  return $results;
}